
from time import time
import networkx as nx
import matplotlib.pyplot as plt


class Underground:
    def __init__(self, underground, location):
        self.underground = []
        self.underground = copy_of_underground(underground)
        self.keys = set()
        self.location = location
        self.graph = None
        self.items = None

        self.extra_requirements = {}
        dummy = {
            'Q': ['x'],
            'P': ['y', 'r'],
            'W': ['e']
        }
        self.forced_moves = {
            'Z': 'x',

            'W': 'E',
            'E': 'b',

            'Q': 'X',
            'X': 'c',
            'c': 'v',

            's': 'B',
            'B': 'd',
            'd': 'D',
            'D': 'h',

            'M': 'k',

            'P': 'Y',
            'Y': 'R',
            'R': 'j',
            'j': 'a',
            'a': 'f',

            'C': 'n',

            'o': 'l',

            'u': 'w',
            'w': 'e',
        }

    def show(self):
        for row in self.underground:
            print(''.join(row))

    def simplify(self):
        simplify_again = True
        while simplify_again:
            simplify_again = False
            for y, row in enumerate(self.underground):
                for x, square in enumerate(row):
                    if (x, y) != self.location and (square == '.' or square.isupper()):
                        if self.count_adj_walls(x, y) == 3:
                            self.underground[y][x] = '#'
                            simplify_again = True

    def build_graph(self):
        self.graph = nx.Graph()
        for y, row in enumerate(self.underground):
            for x, square in enumerate(row):
                if square == '.':
                    node = f'({x},{y})'
                    for dx, dy in [(1,0),(-1,0),(0,1),(0,-1)]:
                        if (x+dx, y+dy) in self.items:
                            item = self.items[(x+dx,y+dy)]
                            self.graph.add_edge(item, node, weight=1)

                    for dx, dy in [(-1, 0), (0, -1)]:
                        if self.underground[y + dy][x + dx] == '.':
                            dest_node = f'({x+dx},{y+dy})'
                            self.graph.add_edge(node, dest_node, weight=1)

    def simplify_graph(self):
        simplify_again = True
        while simplify_again:
            simplify_again = False
            for node in [g for g in self.graph]:
                if node[0] == '(':
                    next_nodes = list(self.graph[node])
                    if len(next_nodes) <= 1:
                        simplify_again = True
                        self.graph.remove_node(node)
                    elif len(next_nodes) == 2:
                        simplify_again = True
                        cost = sum(edge['weight'] for edge in self.graph.adj[node].values())
                        self.graph.add_edge(next_nodes[0], next_nodes[1], weight=cost)
                        self.graph.remove_node(node)

    def find_items(self):
        self.items = dict()
        for y, row in enumerate(self.underground):
            for x, square in enumerate(row):
                if square.isupper() or square.islower():
                    self.items[(x, y)] = square
                elif square == '@':
                    self.items[(x, y)] = square + str((x,y))

    def rec_calc_graph(self, g, item, item_location):
        x, y = item_location
        self.underground[y][x] = '#'
        self.location = item_location
        g.add_node(item, location=item_location)
        reachable = self.get_reachable('')
        for reachable_item in reachable:
            print(item, reachable_item)
            reachable_steps, reachable_location = reachable[reachable_item]
            g.add_edge(item, reachable_item, weight=reachable_steps)
            self.rec_calc_graph(g, reachable_item, reachable_location)

    def calc_graph(self):
        temp_keys = self.keys
        temp_underground = copy_of_underground(self.underground)
        temp_location = self.location
        self.keys = set('abcdefghijklmnopqrstuvwxyz')
        g = nx.Graph()
        g.add_node('@', location=self.location)
        reachable = self.get_reachable('')
        for reachable_item in reachable:
            reachable_steps, reachable_location = reachable[reachable_item]
            print('@', reachable_item)
            g.add_edge('@', reachable_item, weight=reachable_steps)
            ix, iy = reachable[reachable_item][1]
            self.rec_calc_graph(g, reachable_item, reachable_location)
        self.keys = temp_keys
        self.underground = copy_of_underground(temp_underground)
        self.location = temp_location
        return g

    def count_adj_walls(self, x, y):
        count = 0
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            if self.underground[y + dy][x + dx] == '#':
                count += 1
        return count

    def next_singular_path(self, x, y, not_this_location=None):
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            if not_this_location is None or (x+dx, y+dy) != not_this_location:
                if self.underground[y + dy][x + dx] != '#':
                    return x+dx, y+dy
        raise Exception('Hmm, we should not come to this point!')

    def find_forced(self):
        print('find_forced: start')
        forced = dict()
        for y, row in enumerate(self.underground):
            for x, square in enumerate(row):
                if square.isupper() or square.islower():
                    item = square
                    print('item', item, self.count_adj_walls(x, y), x, y)
                    if self.count_adj_walls(x, y) == 3:
                        print('potential')
                        prev_x, prev_y = x, y
                        x, y = self.next_singular_path(x, y)
                        print(x,y,prev_x,prev_y)
                        while self.count_adj_walls(x, y) == 2:
                            dest = self.underground[y][x]
                            print('dest', dest)
                            if dest.islower():
                                break
                            if dest.isupper():
                                forced[dest] = item
                                print('force found', dest, item)
                                item = dest
                            tx, ty = x, y
                            x, y = self.next_singular_path(x, y, (prev_x, prev_y))
                            prev_x, prev_y = tx, ty
                            print(x, y, prev_x, prev_y)
        print('find_forced: stop')
        return forced

    def get_reachable(self, state):
        dist = {self.location: 0}
        reachable = dict()

        while self.update_dist(dist, reachable, state):
            pass
        return reachable

    def update_dist(self, dist, reachable, state):
        max_steps = max(dist.values())
        updated = False
        locations = [x for x in dist.keys()]
        for location in locations:
            steps = dist[location]
            if steps == max_steps:
                x, y = location
                for dx, dy in [(1,0), (0,1), (-1,0), (0,-1)]:
                    square = self.underground[y+dy][x+dx]
                    if square == '.' and (x+dx, y+dy) not in dist:
                        dist[(x+dx, y+dy)] = steps + 1
                        updated = True
                    elif square.islower():
                        add_square = True
                        if square in self.extra_requirements:
                            for r in self.extra_requirements[square]:
                                if r not in state:
                                    add_square = False
                        if add_square:
                            reachable[square] = (steps + 1, (x+dx, y+dy))
                    elif square.isupper() and square.lower() in self.keys:
                        add_square = True
                        if square in self.extra_requirements:
                            for r in self.extra_requirements[square]:
                                if r not in state:
                                    add_square = False
                        if add_square:
                            reachable[square] = (steps + 1, (x + dx, y + dy))
        return updated

    def estimate_needed_steps(self, keys_to_find):
        estimate = 0
        sx, sy = self.location
        for y, row in enumerate(self.underground):
            for x, square in enumerate(row):
                if square in keys_to_find:
                    estimate = max(estimate, abs(sx - x) + abs(sy - y))
        return estimate

    def search_shortest(self, keys_to_find, steps=0, best_so_far=None, state=list(), visited_states=dict()):
        if best_so_far is not None:
            if steps >= best_so_far:
                print(best_so_far, 'ABO', steps, '\t', ''.join(state))
                return best_so_far
            if steps + self.estimate_needed_steps(keys_to_find) > best_so_far:
                print(best_so_far, 'EST', steps, '\t', ''.join(state))
                return best_so_far

        if len(state) >= 2:
            state_string = ''.join(sorted(state[:-1])) + state[-1]
            #print(state_string)
            if state_string in visited_states:
                if steps >= visited_states[state_string]:
                    return best_so_far
            visited_states[state_string] = steps

        reachable = self.get_reachable(state)
        #self.show()
        #print(best_so_far, steps, keys_to_find)
        #print(reachable)

        current_location = self.location
        for item in sorted(reachable, key=lambda tmp: abs(self.location[0]-reachable[tmp][1][0]) + abs(self.location[1]-reachable[tmp][1][1])):
            if len(state) == 0 or state[-1] not in self.forced_moves or item == self.forced_moves[state[-1]]:
                item_steps, item_location = reachable[item]
                item_x, item_y = item_location

                steps += item_steps
                if item in keys_to_find:
                    if len(keys_to_find) == 1:
                        print(f'done {steps}')
                        return steps
                    keys_to_find.remove(item)
                    item_in_keys_to_find = True
                else:
                    item_in_keys_to_find = False
                if item.islower():
                    self.keys.add(item)

                self.underground[item_y][item_x] = '.'
                self.location = item_location

                state.append(item)

                active_steps = self.search_shortest(keys_to_find, steps, best_so_far, state, visited_states)
                if best_so_far is None or active_steps < best_so_far:
                    best_so_far = active_steps

                state.pop()

                # undo the current action
                self.underground[item_y][item_x] = item
                self.location = current_location
                if item.islower():
                    self.keys.remove(item)
                if item_in_keys_to_find:
                    keys_to_find.add(item)
                steps -= item_steps
            del reachable[item]

        return best_so_far


def copy_of_underground(underground):
    copy_of = []
    for row in underground:
        copy_of.append([x for x in row])

    return copy_of


def main():
    tunnel = []

    # [3886, 4538, 4866] is not correct
    # 2000 cant find any solution
    # answer is in 2001-3864

    keys = set()
    with open('day18.data') as file:
        for y, line in enumerate(file):
            row = []
            for x, square in enumerate(line.replace('\n', '')):
                if square == '@':
                    location = (x, y)
                    row.append('@')
                else:
                    if square.islower():
                        keys.add(square)
                    row.append(square)
            tunnel.append(row)

    x, y = location
    for dx in range(3):
        for dy in range(3):
            if dx == 1 or dy == 1:
                tunnel[y + dy - 1][x + dx - 1] = '#'
            else:
                tunnel[y + dy - 1][x + dx - 1] = '@'
    underground = Underground(tunnel, location)

    underground.show()
    underground.simplify()
    underground.show()
    underground.find_items()
    underground.build_graph()
    underground.simplify_graph()
    print('r-p', nx.shortest_path_length(underground.graph, "r", "p", weight="weight"))
    print('@(41, 41)-p', nx.shortest_path_length(underground.graph, "@(41, 41)", "p", weight="weight"))
    print('@(41, 41)-r', nx.shortest_path_length(underground.graph, "@(41, 41)", "r", weight="weight"))
    nx.draw_networkx(underground.graph, node_color='red')
    plt.show()
    return
    g = underground.calc_graph()
    nx.draw_networkx(g, node_color='red')
    plt.show()

    print('looping the edges')
    w = dict()
    for u, v, weight in g.edges.data('weight'):
        print(u,v,weight)
        if u not in w:
            w[u] = weight
        if weight < w[u]:
            w[u] = weight
        if v not in w:
            w[v] = weight
        if weight < w[u]:
            w[v] = weight
    min_cost = sum(w.values())
    print(f'min_cost: {min_cost}')
    print(f'nodes: {g.nodes(data=True)}')
    print(f'edges: {g.edges(data=True)}')
    #return
    print(underground.find_forced())

    print(f'shortest path for all keys: {underground.search_shortest(keys)}')




if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
