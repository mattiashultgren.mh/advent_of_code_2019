
from time import time
import networkx as nx
import matplotlib.pyplot as plt


class World:
    def __init__(self, world):
        self.world = []
        self.world = World.copy_of_world(world)
        self.graph = None
        self.portals = None

    def show(self):
        for row in self.world:
            print(''.join(row))

    def count_adj_walls(self, x, y):
        count = 0
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            if self.world[y + dy][x + dx] == '#':
                count += 1
        return count

    def simplify_world(self):
        simplify_again = True
        while simplify_again:
            simplify_again = False
            for y, row in enumerate(self.world):
                for x, square in enumerate(row):
                    if square == '.':
                        if self.count_adj_walls(x, y) >= 3:
                            self.world[y][x] = '#'
                            simplify_again = True

    def simplify_graph(self):
        simplify_again = True
        while simplify_again:
            simplify_again = False
            for node in [g for g in self.graph]:
                if node not in ['AA','ZZ']:
                    next_nodes = list(self.graph[node])
                    if len(next_nodes) <= 1:
                        simplify_again = True
                        self.graph.remove_node(node)
                    elif len(next_nodes) == 2:
                        simplify_again = True
                        cost = sum(edge['weight'] for edge in self.graph.adj[node].values())
                        self.graph.add_edge(next_nodes[0], next_nodes[1], weight=cost)
                        self.graph.remove_node(node)

    def find_portals(self):
        self.portals = dict()
        for y, row in enumerate(self.world):
            for x, square in enumerate(row):
                if square.isupper():
                    for dx, dy in [(1,0), (0,1)]:
                        tx, ty = x+dx, y+dy
                        if ty < len(self.world) and tx < len(self.world[ty]):
                            second_letter = self.world[ty][tx]
                            if second_letter.isupper():
                                self.portals[(x, y)] = ''.join((square, second_letter))
                                self.portals[(tx, ty)] = ''.join((square, second_letter))

    def build_graph(self):
        self.graph = nx.Graph()
        for y, row in enumerate(self.world):
            for x, square in enumerate(row):
                if square == '.':
                    node = f'({x},{y})'
                    for dx, dy in [(1,0),(-1,0),(0,1),(0,-1)]:
                        if self.world[y+dy][x+dx].isupper():
                            p = self.portals[(x+dx,y+dy)]
                            self.graph.add_edge(p, node, weight=0.5)

                    for dx, dy in [(-1, 0), (0, -1)]:
                        if self.world[y + dy][x + dx] == '.':
                            dest_node = f'({x+dx},{y+dy})'
                            self.graph.add_edge(node, dest_node, weight=1)

    def build_recursive_graph(self, max_depth):
        self.graph = nx.Graph()
        for z in range(max_depth+1):
            for y, row in enumerate(self.world):
                for x, square in enumerate(row):
                    if square == '.':
                        node = f'({x},{y},{z})'
                        for dx, dy in [(1,0),(-1,0),(0,1),(0,-1)]:
                            tx, ty = x+dx, y+dy
                            if self.world[ty][tx].isupper():
                                if 20 < tx < 110 and 20 < ty < 110:
                                    tz = z + 1
                                else:
                                    tz = z - 1
                                p = self.portals[(tx, ty)]
                                if 0 <= tz <= max_depth:
                                    self.graph.add_edge(p + str(tz), node, weight=0.5)
                                elif tz == -1 and p in ['AA', 'ZZ']:
                                    self.graph.add_edge(p, node, weight=0.5)
                            elif self.world[ty][tx] == '.':
                                self.graph.add_edge(node, f'({tx},{ty},{z})', weight=1)

    @staticmethod
    def copy_of_world(world):
        copy_of = []
        for row in world:
            copy_of.append([x for x in row])
        return copy_of


def main():
    karta = []
    with open('day20.data') as file:
        for y, line in enumerate(file):
            row = []
            for x, square in enumerate(line.replace('\n', '')):
                row.append(square)
            karta.append(row)

    world = World(karta)
    world.simplify_world()
    world.show()
    world.find_portals()

    for part in [1, 2]:
        if part == 1:
            world.build_graph()
        else:
            world.build_recursive_graph(80)
        print(f'nodes: {len(world.graph)}')
        print(f'part{part}: path_length: {int(nx.shortest_path_length(world.graph, "AA", "ZZ", weight="weight") - 1)}')
        world.simplify_graph()
        print(f'nodes: {len(world.graph)}')
        nx.draw_networkx(world.graph, node_color='red')
        plt.show()


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
