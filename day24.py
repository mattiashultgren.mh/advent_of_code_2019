
from time import time


def get_value(state):
    value = 0
    for x, y in state:
        value |= 2**(x + y*5)
    return value


def evolve(state):
    new_state = set()
    for y in range(5):
        for x in range(5):
            count = 0
            for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                count += 1 if (x+dx, y+dy) in state else 0
            if (x, y) in state:
                if count == 1:
                    new_state.add((x, y))
            else:
                if 1 <= count <= 2:
                    new_state.add((x, y))
    return new_state


def evolve_from(state, state_below, state_above):
    #print('evolve_from\n')
    #print(get_str_state(state_below) + '\n')
    #print(get_str_state(state) + '\n')
    #print(get_str_state(state_above) + '\n')
    new_state = set()
    for y in range(5):
        temp = ''
        for x in range(5):
            if x == 2 and y == 2:
                temp += '  '
                continue
            count = 0
            for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                count += 1 if (x + dx, y + dy) in state else 0
                if x in [0, 4] or y in [0, 4]:
                    if x == 0 and dx == -1:
                        count += 1 if (1, 2) in state_above else 0
                    elif y == 0 and dy == -1:
                        count += 1 if (2, 1) in state_above else 0
                    elif x == 4 and dx == 1:
                        count += 1 if (3, 2) in state_above else 0
                    elif y == 4 and dy == 1:
                        count += 1 if (2, 3) in state_above else 0
                elif x == 2 or y == 2:
                    for r in range(5):
                        if x == 1 and y == 2 and dx == 1:
                            count += 1 if (0, r) in state_below else 0
                        elif y == 1 and x == 2 and dy == 1:
                            count += 1 if (r, 0) in state_below else 0
                        elif x == 3 and y == 2 and dx == -1:
                            count += 1 if (4, r) in state_below else 0
                        elif y == 3 and x == 2 and dy == -1:
                            count += 1 if (r, 4) in state_below else 0

            temp += str(count) + ' '
            if (x, y) in state:
                if count == 1:
                    new_state.add((x, y))
            else:
                if 1 <= count <= 2:
                    new_state.add((x, y))
        #print(temp)
    #print()

    return new_state


def evolve_rec(states):
    new_states = dict()

    min_depth = min(states)
    max_depth = max(states)

    temp = evolve_from(dict(), dict(), states[min_depth])
    if len(temp) > 0:
        new_states[min_depth-1] = temp

    temp = evolve_from(dict(), states[max_depth], dict())
    if len(temp) > 0:
        new_states[max_depth+1] = temp

    for d in states:
        new_states[d] = evolve_from(states[d],
                                    states.get(d-1, dict()),
                                    states.get(d+1, dict()))
    return new_states


def get_str_state(state):
    str_state = ''
    for y in range(5):
        for x in range(5):
            str_state += '#' if (x, y) in state else '.'
        str_state += '\n'
    return str_state


def data_to_state(data):
    state = set()
    for y in range(5):
        for x in range(5):
            if data[y][x] == '#':
                state.add((x, y))
    return state


def part1(data):
    state = data_to_state(data)

    visited = {get_value(state)}
    while True:
        state = evolve(state)
        value = get_value(state)
        if value in visited:
            print(f'{value} appears twice first')
            break
        visited.add(value)


def part2(data, minutes):
    states = {0: data_to_state(data)}

    for m in range(minutes):
        states = evolve_rec(states)

    # flip the printing order as this code has worked in the opposite order as the AoC did...
    #for d in reversed(sorted(states)):
    #    print(f'Depth {-d}:')
    #    if d in states:
    #        print(get_str_state(states[d]))
    bugs = sum(len(state) for state in states.values())
    print(f'number of bugs after {m+1} minutes is {bugs}')


def main(todo):
    if todo == 'real data':
        data = ['#.#..',
                '.#.#.',
                '#...#',
                '.#..#',
                '##.#.']
        part1(data)
        part2(data, 200)
    else:
        data = ['....#',
                '#..#.',
                '#..##',
                '..#..',
                '#....']
        part1(data)
        part2(data, 10)


if __name__ == '__main__':
    start = time()
    main('real data')
    print(f'Time: {time()-start:.3}')
