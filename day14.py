
from time import time


class Chemical:
    def __init__(self, name, count):
        self.name = name
        self.count = count


class Reaction:
    def __init__(self, input_chemicals, output_chemical):
        self.inputs = input_chemicals
        self.output = output_chemical


def simplify_state(state):
    # clump everything together to the left
    for i in range(len(state)):
        for ii in range(i+1, len(state)):
            if state[i].name == state[ii].name:
                state[i].count += state[ii].count
                state[ii].count = 0
    # remove chemicals with 0 count
    for i in range(len(state) - 1, -1, -1):
        if state[i].count == 0:
            state.remove(state[i])


def is_state_ore_only(state):
    ore_found = False
    for chemical in state:
        if chemical.count > 0:
            if chemical.name == 'ORE':
                ore_found = True
            else:
                return False
    return ore_found


def expand_first_demand(state, reactions):
    if state[0].name == 'ORE' or state[0].count <= 0:
        state.append(state[0])
        state.remove(state[0])
    else:
        for r in reactions:
            if state[0].name == r.output.name:
                factor = (state[0].count + r.output.count - 1) // r.output.count
                state[0].count -= factor * r.output.count
                add(state, r.inputs, factor)
                return


def compute_ore_demand(state, reactions):
    while not is_state_ore_only(state):
        expand_first_demand(state, reactions)
    return state


def add(state, fuel_demand, factor):
    for chemical in fuel_demand:
        state.append(Chemical(chemical.name, chemical.count * factor))
    simplify_state(state)


def main():
    data = [line.replace('\n', '').split('=>') for line in open('day14.data')]
    reactions = []
    for d in data:
        d[0] = d[0].split(',')
        input_chemicals = []
        for q in d[0]:
            temp = q.strip().split(' ')
            num = int(temp[0])
            typ = temp[1]
            input_chemicals.append(Chemical(typ, num))
        temp = d[1].strip().split(' ')
        num = int(temp[0])
        typ = temp[1]
        if typ == 'FUEL':
            fuel_demand = input_chemicals

        else:
            reactions.append(Reaction(input_chemicals, Chemical(typ, num)))

    ore_limit = 10**12
    num_fuel = 1
    state = []
    add(state, fuel_demand, 1)

    compute_ore_demand(state, reactions)
    for chemical in state:
        if chemical.name == 'ORE':
            ore_left = ore_limit - chemical.count
            print(f'{chemical.count} ore usage for {num_fuel} fuel')

    for ex in range(12, -1, -1):
        while ore_left > 0:
            restore_point = [num_fuel, []]
            add(restore_point[1], state, 1)
            factor = 10**ex
            num_fuel += factor
            add(state, fuel_demand, factor)
            compute_ore_demand(state, reactions)
            for chemical in state:
                if chemical.name == 'ORE':
                    ore_left = ore_limit - chemical.count
        state = []
        add(state, restore_point[1], 1)
        num_fuel = restore_point[0]
        ore_left = 1
    print(f'we can get maximum {num_fuel} fuel with maximum {ore_limit} ore')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
