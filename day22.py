
from time import time


def deal_into_new_stack(cards):
    return [x for x in reversed(cards)]


def cut(cards, n):
    new_stack = cards[n:]
    new_stack.extend(cards[0:n])
    return new_stack


def deal_with_increment(cards, n):
    new_stack = [0] * len(cards)
    index = 0
    for x in cards:
        new_stack[index] = x
        index = (index + n) % len(new_stack)
    return new_stack


def backward_deal_with_increment(num_cards, n, position):
    # solve    A * n = B * num_cards + position
    # or       A * n = position (mod num_cards)
    # where A is the new position
    g, x, y = gcd_extended(n, num_cards)
    return (position * x) % num_cards


def shuffle(cards, actions):
    str_cut = 'cut '
    str_deal_with = 'deal with increment '
    str_deal_into = 'deal into new stack'
    for action in actions:
        if action.startswith(str_cut):
            cards = cut(cards, int(action[len(str_cut):]))
        elif action.startswith(str_deal_with):
            cards = deal_with_increment(cards, int(action[len(str_deal_with):]))
        elif action == str_deal_into:
            cards = deal_into_new_stack(cards)
    return cards


def get_backward_formula(num_cards, actions):
    str_cut = 'cut '
    str_deal_with = 'deal with increment '
    str_deal_into = 'deal into new stack'
    k, m = 1, 0
    for action in reversed(actions):
        if action.startswith(str_cut):
            m += int(action[len(str_cut):])
        elif action.startswith(str_deal_with):
            factor = backward_deal_with_increment(num_cards, int(action[len(str_deal_with):]), 1)
            k *= factor
            m *= factor
        elif action == str_deal_into:
            k = -k
            m = -m - 1
        k %= num_cards
        m %= num_cards
    return k, m


def gcd_extended(a, b):
    if a == 0:
        return b, 0, 1
    g, x, y = gcd_extended(b % a, a)
    return g, y - (b // a) * x, x


def run_tests():
    cards = [x for x in range(10)]
    print(deal_into_new_stack(cards))
    print(cut(cards, 3))
    print(cut(cards, -4))
    print(deal_with_increment(cards, 3))

    print()
    actions = [line.replace('\n', '') for line in open('day22.test.data')]
    cards = shuffle(cards, actions)
    print(cards)
    cards = shuffle(cards, actions)
    print(cards)
    cards = shuffle(cards, actions)
    print(cards)

    print('formula', get_backward_formula(10, actions))


def main():
    #run_tests()

    actions = [line.replace('\n', '') for line in open('day22.data')]
    cards = [x for x in range(10007)]
    cards = shuffle(cards, actions)
    pos_card_2019 = cards.index(2019)
    print(f'part1: position of card 2019: {pos_card_2019}')
    k, m = get_backward_formula(len(cards), actions)
    card = (k*pos_card_2019 + m) % len(cards)
    print(f'part1 in reverse, card at position {pos_card_2019} is {card}')

    start_position = 2020
    num_cards = 119315717514047
    iterations = 101741582076661

    k, m = get_backward_formula(num_cards, actions)
    print(f'formula: p(n-1) = {k}*p(n) + {m}')

    position = start_position
    while iterations > 0:
        # if last bit of iterations is set then we apply the current formula
        if iterations & 1 == 1:
            position = (k*position + m) % num_cards

        # shift away the lowest bit
        iterations //= 2

        # apply the formula to itself. which doubles the iterations it describes
        k, m = k**2, k*m + m
        k %= num_cards
        m %= num_cards

    print(f'part2: card at position {start_position} is: {position}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
