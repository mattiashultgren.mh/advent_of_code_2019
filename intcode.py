
from queue import Queue
from collections import defaultdict


class IntCode:
    def __init__(self, memory):
        self.pc = 0
        self.rel_base = 0
        self.memory = defaultdict(int)
        for addr, value in enumerate(memory):
            self.memory[addr] = value
        self.running = False
        self.input_queue = Queue()
        self.yield_data = None

        self.opcodes = {
            1: (lambda x, y: x + y, 2, True),
            2: (lambda x, y: x * y, 2, True),
            3: (lambda: self.input_queue.get(), 0, True),
            4: (lambda x: setattr(self, 'yield_data', x), 1, False),
            5: (self.instr_jump_if_true, 2, False),
            6: (self.instr_jump_if_false, 2, False),
            7: (lambda x, y: 1 if x < y else 0, 2, True),
            8: (lambda x, y: 1 if x == y else 0, 2, True),
            9: (lambda x: setattr(self, 'rel_base', self.rel_base + x), 1, False),
            99: (lambda: setattr(self, 'running', False), 0, False),
        }
        self.addr_mode = {
            0: lambda i: self.memory[self.pc + i + 1],
            1: lambda i: self.pc + i + 1,
            2: lambda i: self.rel_base + self.memory[self.pc + i + 1],
        }

    def instr_jump_if_true(self, x, y):
        if x != 0:
            self.pc = y

    def instr_jump_if_false(self, x, y):
        if x == 0:
            self.pc = y

    def set_input(self, new_input):
        self.input_queue = new_input

    def execute(self):
        self.pc = 0
        self.rel_base = 0
        self.running = True

        while self.running:
            instruction = self.memory[self.pc]
            opcode = instruction % 100
            opfunc, parameters, store_result = self.opcodes[opcode]
            p = []
            for i in range(parameters + store_result):
                mode = (instruction // (100 * 10**i)) % 10
                addr = self.addr_mode[mode](i)
                if i != parameters:
                    p.append(self.memory[addr])

            self.pc += 1 + parameters + store_result

            if store_result:
                self.memory[addr] = opfunc(*p)
            else:
                opfunc(*p)

            if self.yield_data is not None:
                yield self.yield_data
                self.yield_data = None
