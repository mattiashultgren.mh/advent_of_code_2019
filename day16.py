
from time import time


def fft_phase(numbers, offset):
    out_numbers = [0] * len(numbers)
    midpoint = max(offset, (len(numbers) + 1) // 2)
    for i in range(offset, midpoint):
        n = 0
        for ii in range(i, len(numbers), 4*(i+1)):
            n += sum(numbers[ii:ii + i + 1])
        for ii in range(2+3*i, len(numbers), 4*(i+1)):
            n -= sum(numbers[ii:ii + i + 1])
        out_numbers[i] = abs(n) % 10

    n = sum(numbers[midpoint-1:])
    for i in range(midpoint, len(numbers)):
        n -= numbers[i-1]
        out_numbers[i] = abs(n) % 10
    return out_numbers


def fft(number, multiplicity, phases, offset):
    numbers = [int(x) for x in number]

    numbers *= multiplicity

    for phase in range(phases):
        numbers = fft_phase(numbers, offset)
    return numbers[offset:]


def main():
    number = '59704438946400225486037825889922820489843190285276623851650874501661128988396696069718826434708024511422795921838800269789913960190601300910423350290846455187315936154437526204822336114717910853157866334743979157700934791877134865819338701289349073169567308015162696370931073040617799608862983736292169088603858502137085782889297989277130087242942506416164598910622349994697403064628500493847458293153920207889114082230150603182206031692080645433361960358161328125435922180533297727179785114625861941781083443388701883640778753411135944703959349861504604264349715262460922987816868400261327556306957183739232107401756998929158348201149705670138765039'
    #number = '12345678'

    numbers = fft(number, 1, 100, 0)
    print(f'Part1: {"".join(map(str,numbers[:8]))}')

    offset = int(number[:7])
    numbers = fft(number, 10000, 100, offset)
    print(f'Part2: {"".join(map(str,numbers[:8]))}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
