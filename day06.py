

def parse_map(orbit_map):
    orbit_dict = dict()
    while len(orbit_map) > 0:
        for idx in range(len(orbit_map)):
            if idx >= len(orbit_map):
                break
            c, o = orbit_map[idx]
            if c == 'COM':
                orbit_dict[c] = (0, None)
            if c in orbit_dict:
                orbit_dict[o] = (orbit_dict[c][0] + 1, c)
                del orbit_map[idx]
    return orbit_dict


def get_path(orbit_dict, k):
    path = []
    while k is not None:
        path.append(k)
        k = orbit_dict[k][-1]
    return path


def main():
    orbit_map = [tuple(line.replace('\n', '').split(')')) for line in open('day06.data', 'r')]
    #orbit_map = [tuple(x.split(')')) for x in ['COM)B','B)C','C)D','D)E','E)F','B)G','G)H','D)I','E)J','J)K','K)L',
    #                                           'K)YOU','I)SAN']]
    orbit_dict = parse_map(orbit_map)
    print(f'total number of orbits; {sum(x[0] for x in orbit_dict.values())}')

    path_you = get_path(orbit_dict, 'YOU')
    path_santa = get_path(orbit_dict, 'SAN')
    # trim the end that is shared after the connection node
    while path_you[-2] == path_santa[-2]:
        del path_you[-1]
        del path_santa[-1]
    print(f'orbit transfers required: {len(path_you) + len(path_santa) - 4}')
    # minus 4 because: YOU, SAN, one_shared, start_orbiting


if __name__ == '__main__':
    main()
