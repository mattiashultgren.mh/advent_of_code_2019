

def trace_wire(desc):
    wire = dict()
    x, y, t = 0, 0, 0
    for direction in desc:
        step = 1 if direction[0] in ['R', 'U'] else -1
        dx = step if direction[0] in ['R', 'L'] else 0
        dy = step if dx == 0 else 0
        for _ in range(int(direction[1:])):
            t += 1
            x += dx
            y += dy
            wire[(x, y)] = t
    return wire


def find_interesting_points(wires):
    for idx in range(len(wires)):
        wires[idx] = trace_wire(wires[idx])

    crossings = set(wires[0].keys()) & set(wires[1].keys())

    closest, fastest = None, None
    for c in crossings:
        x, y = c
        distance = abs(x) + abs(y)
        timing = wires[0][c] + wires[1][c]
        if closest is None or closest > distance:
            closest = distance
        if fastest is None or fastest > timing:
            fastest = timing
    return closest, fastest


def main():
    wires = [line.split(',') for line in open('day03.data', 'r')]
#    wires = ['R8,U5,L5,D3'.split(','),
#             'U7,R6,D4,L4'.split(',')]
#    wires = ['R75,D30,R83,U83,L12,D49,R71,U7,L72'.split(','),
#             'U62,R66,U55,R34,D71,R55,D58,R83'.split(',')]
    closest, fastest = find_interesting_points(wires)
    print(f'closest: {closest}   fastest: {fastest}')


if __name__ == '__main__':
    main()
