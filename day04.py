

def is_valid(n, ignore_double_in_triple):
    double = False
    in_triple = False
    while n > 0:
        last = n % 10
        second = (n // 10) % 10
        third = (n // 100) % 10

        if last < second:
            return False
        elif ignore_double_in_triple and last == second and last == third:
            in_triple = True
        elif last == second and not in_triple:
            double = True
        else:
            in_triple = False

        n //= 10
    return double


def main():
    count_part1 = 0
    count_part2 = 0
    for n in range(231832, 767346):
        if is_valid(n, False):
            count_part1 += 1
            if is_valid(n, True):
                count_part2 += 1
    print(f'Part1: {count_part1}')
    print(f'Part2: {count_part2}')


if __name__ == '__main__':
    main()
