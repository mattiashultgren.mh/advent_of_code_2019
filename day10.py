
import time
import math


def is_line_of_sight(asteroids, a1, a2):
    dx, dy = a2[0] - a1[0], a2[1] - a1[1]
    div = math.gcd(dx, dy)
    dx, dy = dx // div, dy // div

    for f in range(1, 10**100):
        x, y = a1[0] + dx*f, a1[1] + dy*f
        if (x, y) == a2:
            return True
        if (x, y) in asteroids:
            return False


def asteroids_in_sight(origin, asteroids):
    for dest in asteroids:
        if origin != dest and is_line_of_sight(asteroids, origin, dest):
            yield dest


def fill_line_of_sight_data(asteroids):
    for src in asteroids:
        asteroids[src] = sum(1 for _ in asteroids_in_sight(src, asteroids))


def find_asteroids(karta):
    w, h = len(karta[0]), len(karta)
    asteroids = dict()
    for y in range(h):
        for x in range(w):
            if karta[y][x] == '#':
                asteroids[(x, y)] = None
    return asteroids


def get_angle(a1, a2):
    dx, dy = a2[0] - a1[0], a2[1] - a1[1]
    return math.atan2(-dx, dy)


def get_rotation_needed(current, wanted):
    if current < wanted:
        return wanted - current
    else:
        return 2*math.pi + wanted - current


def vaporize(asteroids, origin):
    order_of_destruction = []
    for y in range(origin[1], -1, -1):
        if (origin[0], y) in asteroids:
            next_to_vaporize = (origin[0], y)
            break
    while len(asteroids) > 0:
        vaporize_this, next_to_vaporize = next_to_vaporize, None
        if vaporize_this is not None:
            angle = get_angle(origin, vaporize_this)
        to_rotate = 2*math.pi
        for ast in asteroids:
            if ast != vaporize_this and is_line_of_sight(asteroids, origin, ast):
                tmp_to_rotate = get_rotation_needed(angle, get_angle(origin, ast))
                if tmp_to_rotate <= to_rotate:
                    to_rotate = tmp_to_rotate
                    next_to_vaporize = ast
        if vaporize_this is not None:
            del asteroids[vaporize_this]
            order_of_destruction.append(vaporize_this)
    return order_of_destruction


def main():
    karta = [line.replace('\n', '') for line in open('day10.data', 'r')]
    #karta = ['.#..#', '.....', '#####', '....#', '...##']
    #karta = [line.replace('\n', '') for line in open('day10.test.data', 'r')]

    asteroids = find_asteroids(karta)
    fill_line_of_sight_data(asteroids)
    max_value = 0
    max_position = None
    for key, value in asteroids.items():
        if value > max_value:
            max_value = value
            max_position = key
    print(f'Part1: position{max_position} has {max_value} asteroids in sight')

    del asteroids[max_position]
    order_of_destruction = vaporize(asteroids, max_position)
    x, y = order_of_destruction[199]
    print('Part2', 100*x + y)


if __name__ == '__main__':
    start = time.time()
    main()
    print(f'Time: {time.time()-start:.3}')
