# Advent of Code 2019

## Day 1 to 16
Solved.

## Day 17
Solved by assuming all crossings can be taken by going straight through.
Then the path was split to the three programs manually.

## Day 18
Solved part 1 by manually finding forced partial sequences then letting the code solve.
Part 2 was solved fully manual.

## Day 19
Solved.

## Day 20
Solved by manually trying maximum recursion depths until a solution was found by the code.

## Day 21
Solved by manually creating the spring-script code by trial and error.

## Day 22 to 24
Solved.

## Day 25
Added auto map with command 'map'.
Played the game.