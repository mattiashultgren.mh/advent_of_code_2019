
from time import time
import math
from functools import reduce


class Moon:
    def __init__(self, x, y, z):
        self.position = [x, y, z]
        self.velocity = [0, 0, 0]

    def potential_energy(self):
        return sum(abs(x) for x in self.position)

    def kinetic_energy(self):
        return sum(abs(x) for x in self.velocity)

    def update_position(self):
        for i in range(3):
            self.position[i] += self.velocity[i]

    def apply_gravity(self, moon2):
        for i in range(3):
            if self.position[i] != moon2.position[i]:
                delta = 1 if self.position[i] < moon2.position[i] else -1
                self.velocity[i] += delta
                moon2.velocity[i] -= delta


def update_velocities(moons):
    for idx, m1 in enumerate(moons):
        for m2 in moons[idx+1:]:
            m1.apply_gravity(m2)


def update_positions(moons):
    for moon in moons:
        moon.update_position()


def total_energy(moons):
    return sum(moon.potential_energy() * moon.kinetic_energy() for moon in moons)


def total_static_axis(moons, axis):
    return tuple((moon.position[axis], moon.velocity[axis]) for moon in moons)


def lcm(a, b):
    return a * b // math.gcd(a, b)


def main():
    moons = [Moon(17, -7, -11), Moon(1, 4, -1), Moon(6, -2, -6), Moon(19, 11, 9)]

    start_positions = [total_static_axis(moons, i) for i in range(3)]
    configurations = [set(), set(), set()]
    periods = [None, None, None]
    results_left = 4
    for steps in range(10**100):
        if steps == 1000:
            print(f'Total energy {total_energy(moons)}')
            results_left -= 1
        for i in range(3):
            if periods[i] is None:
                position = total_static_axis(moons, i)
                if position in configurations[i]:
                    if position != start_positions[i]:
                        raise Exception("We didn't get back to the start position, bad news as we don't support this!")
                    periods[i] = steps
                    results_left -= 1
                configurations[i].add(position)

        update_velocities(moons)
        update_positions(moons)

        if results_left == 0:
            print(f'system full period is: {reduce(lcm, periods)}')
            break


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
