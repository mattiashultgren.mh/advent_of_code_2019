

def req_fuel(m):
    return (m//3) - 2


def req_module_fuels(m):
    while m > 0:
        m = req_fuel(m)
        if m > 0:
            yield m


def part1(weights):
    return sum(req_fuel(w) for w in weights)


def part2(weights):
    total_fuel = 0
    for w in weights:
        total_fuel += sum(req_module_fuels(w))
    return total_fuel


def main():
    weights = [int(line) for line in open('day01.data', 'r')]
    print(f'Part1: Total fuel: {part1(weights)}')
    print(f'Part2: Total fuel: {part2(weights)}')


if __name__ == '__main__':
    main()
